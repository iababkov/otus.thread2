﻿using System;

namespace OtusThread
{
    class IntArrayBuilder
    {
        public static int[] GetRandom(int numberOfIntegers, int maxInteger)
        {
            int[] _integers = new int[numberOfIntegers];

            var intGenerator = new Random();

            for (int i = 0; i < numberOfIntegers; i++)
            {
                _integers[i] = intGenerator.Next(maxInteger);
            }

            return _integers;
        }
    }
}