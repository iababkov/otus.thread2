﻿using System;
using System.Linq;
using System.Threading;

namespace OtusThread
{
    abstract class IntArrayOperations
    {
        protected static int[] _integers;
        protected static int _sumOfIntegers;

        public IntArrayOperations(int[] integers)
        {
            _integers = integers;
        }

        abstract public int SumUp();
    }

    class IntArrayOperations_Simple : IntArrayOperations
    {
        public IntArrayOperations_Simple(int[] integers) : base(integers)
        {
            _integers = integers;
        }

        override public int SumUp()
        {
            int _sumOfIntegers = 0;

            for (int i = 0; i < _integers.Length; i++)
            {
                _sumOfIntegers += _integers[i];
            }

            return _sumOfIntegers;
        }
    }

    class IntArrayOperations_Thread : IntArrayOperations
    {
        private readonly int _numberOfThreads;
        private static CountdownEvent _loadFinishedEvent;

        public IntArrayOperations_Thread(int[] integers, int numberOfThreads) : base(integers)
        {
            _integers = integers;
            _numberOfThreads = numberOfThreads;
        }

        private void SumUpInThread(object parameter)
        {
            if (parameter is Tuple<int, int> rangeTuple)
            {
                for (int i = rangeTuple.Item1; i < rangeTuple.Item2; i++)
                {
                    Interlocked.Add(ref _sumOfIntegers, _integers[i]);
                }
            }

            if (_loadFinishedEvent != null)
            {
                _loadFinishedEvent.Signal();
            }
        }

        override public int SumUp()
        {
            _sumOfIntegers = 0;

            int threadCapacity = _integers.Length / _numberOfThreads + (_integers.Length % _numberOfThreads > 0 ? 1 : 0);

            _loadFinishedEvent = new CountdownEvent(1);

            for (int i = 0; i < _numberOfThreads; i++)
            {
                {
                    int firstNumberIndex = i * threadCapacity;
                    int lastNumberIndex = firstNumberIndex + threadCapacity;

                    _loadFinishedEvent.AddCount();

                    Thread myThread = new(new ParameterizedThreadStart(SumUpInThread));
                    myThread.Start(Tuple.Create(firstNumberIndex, lastNumberIndex > _integers.Length ? _integers.Length : lastNumberIndex));
                }
            }

            _loadFinishedEvent.Signal();
            _loadFinishedEvent.Wait();

            return _sumOfIntegers;
        }
    }

    class IntArrayOperations_PLinq : IntArrayOperations
    {
        public IntArrayOperations_PLinq(int[] integers) : base(integers)
        {
            _integers = integers;
        }

        override public int SumUp()
        {
            return _integers.AsParallel().Sum();
        }
    }
}