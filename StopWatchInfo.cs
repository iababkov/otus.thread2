﻿using System;
using System.Diagnostics;

namespace OtusThread
{
    class StopWatchInfo
    {
        private Stopwatch _stopWatch;

        public void Start()
        {
            if (_stopWatch == null)
            {
                _stopWatch = new();
            }
            else
            {
                _stopWatch.Reset();
            }

            _stopWatch.Start();
        }

        public void Stop_IntValue(int value)
        {
            _stopWatch.Stop();
            Console.WriteLine($"Processing time: {_stopWatch.Elapsed}");
            Console.WriteLine($"Result: {value}");
        }
    }
}