﻿using System;

namespace OtusThread
{
    class Program
    {
        static void Main()
        {
            StopWatchInfo stopWatchSumUp = new();

            for (int i = 0; i < 3; i++)
            {
                int numberOfIntegers = 100000 * (int)Math.Pow(10, i);

                int[] integers = IntArrayBuilder.GetRandom(numberOfIntegers, 10);

                Console.WriteLine($"Number of integers: {numberOfIntegers}");

                stopWatchSumUp.Start();
                stopWatchSumUp.Stop_IntValue(new IntArrayOperations_Simple(integers).SumUp());

                stopWatchSumUp.Start();
                stopWatchSumUp.Stop_IntValue(new IntArrayOperations_Thread(integers, Environment.ProcessorCount).SumUp());

                stopWatchSumUp.Start();
                stopWatchSumUp.Stop_IntValue(new IntArrayOperations_PLinq(integers).SumUp());
            }
        }
    }
}